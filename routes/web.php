<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', 'App\Http\Controllers\IndexController')->name('index');
Route::post('/', 'App\Http\Controllers\AddXML\StoreController')->name('xml.store');
Route::get('/products', 'App\Http\Controllers\Product\IndexController')->name('product.index');
Route::get('/xlsx_download', 'App\Http\Controllers\Xlsx\GetFileController')->name('xlsx.load');


