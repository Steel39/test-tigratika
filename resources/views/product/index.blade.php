@extends('main.main')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0"></h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">

                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Список товаров</h3>
                                <div class="card-tools">
                                    <div class="input-group input-group-sm">
                                        <a href="{{ route('xlsx.load') }}">
                                            <input type="button", class="btn-dark" value="Загрузить xlsx">
                                        </a>
                                    </div>
                                </div>

                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>Название</th>
                                        <th>Категория</th>
                                        <th>Подкатегория</th>
                                        <th>Доступность</th>
                                        <th>ID товара</th>
                                        <th>Цена</th>
                                        <th>Старая цена</th>
                                        <th>Ссылка на товар</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($products as $product)
                                    <tr>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->category }}</td>
                                        <td>{{ $product->sub_category }}</td>
                                        <td>{{ $product->available }}</td>
                                        <td>{{ $product->product_id }}</td>
                                        <td>{{ $product->price }}</td>
                                        <td>{{ $product->old_price }}</td>
                                        <td><a href="{{ $product->url }}">{{ $product->url }}</a></td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </div>
    </div>
@endsection
