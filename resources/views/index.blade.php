@extends('main.main')
@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Создание списка продуктов</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <h3></h3>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
                <section class="content">
                    <div class="container-fluid">
                        <!-- Small boxes (Stat box) -->
                        <div class="row">
                            <div class="col-12">
                                <form action="{{ route('xml.store') }}"  method="POST" class="w-25">
                                    @csrf
                                    <div class="form-group">
                                        <label ></label>
                                        <input type="text" class="form-control" name="xml_url" placeholder="Добавьте ссылку на Ваш XML...">
                                        @error('xml_url')
                                        <div class="text-danger mb-2" >Это поле не должно быть пустым</div>
                                        @enderror
                                    </div>
                                    <a href="{{ route('index') }}"><input type="submit" class="btn btn-primary" value="Добавить"></a>
                                </form>
                            </div>
                            <!-- /.row -->
                        </div><!-- /.container-fluid -->
                    </div>
                </section>
            </div><!-- /.container-fluid -->
        </div>
    </div>

    <!-- /.content -->

@endsection
