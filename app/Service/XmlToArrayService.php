<?php

namespace App\Service;

class XMLToArrayService
{
    public array $arrayDataProducts;
    public array $arrayDataCategories;
    public string $available;

    public function XMLToArrayProducts($simpleObj): array
    {
        if (isset($simpleObj)) {
            foreach ($simpleObj as $key => $value) {
                $product_id = strval($value->attributes()->id);
                if ($value->attributes()->available == true) {
                    $this->available = 'В наличии';
                } else {
                    $this->available = 'Нет в наличии';
                }
                $url = strval($value->url);
                $price = strval($value->price);
                $oldPrice = strval($value->oldprice);
                $currencyId = strval($value->currencyId);
                $categoryId = strval($value->categoryId);
                $picture = strval($value->picture);
                $name = strval($value->name);
                $this->arrayDataProducts[] = [
                    'product_id' => $product_id,
                    'name' => $name,
                    'url' => $url,
                    'price' => $price,
                    'old_price' => $oldPrice,
                    'currency_id' => $currencyId,
                    'category_id' => $categoryId,
                    'picture' => $picture,
                    'available' => $this->available
                ];

            }
        }
        return $this->arrayDataProducts;
    }

    public function XMLToArrayCategories($simpleObj): array
    {
        if (isset($simpleObj)) {
            foreach ($simpleObj as $key => $value) {
                $name = strval($value);
                $id = strval($value->attributes()->id);
                $parentId = strval($value->attributes()->parentId);

                $this->arrayDataCategories[$id] = [
                    'name' => $name,
                    'parent_id' => $parentId,
                ];
            }
        }

        return $this->arrayDataCategories;
    }




}
