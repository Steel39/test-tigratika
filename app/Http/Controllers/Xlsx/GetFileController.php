<?php

namespace App\Http\Controllers\Xlsx;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Service\ExcelService;
use Illuminate\Http\Request;

class GetFileController extends Controller
{
    public function __invoke(ExcelService $service, Product $product)
    {
        $data = $product->get()->values()->all();
        $service->arrayToXlsx($data);
    }
}
