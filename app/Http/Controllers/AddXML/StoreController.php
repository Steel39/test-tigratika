<?php

namespace App\Http\Controllers\AddXML;

use App\Http\Controllers\Controller;
use App\Http\Requests\XMLRequest;
use App\Service\ProductStoreService;
use App\Service\XmlDataService;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    public function __invoke(XMLRequest $request, XmlDataService $service, ProductStoreService $store)
    {
        $data = $request->validated();
        $file = $service->getFile($data);
        $arrayProducts = $service->getListProduct($file);
        $arrayCategories = $service->getCategories($file);
        $dataStore = $service->getDataStore($arrayProducts, $arrayCategories);
        $store->storeProduct($dataStore);
        return redirect(route('product.index'));
    }
}

